;;;; abcl-json.lisp

(in-package #:abcl-json)


(defun string->json (string)
  (java:jcall (java:jmethod "com.google.gson.JsonParser" "parse" "java.lang.String")
              (java:jnew "com.google.gson.JsonParser") string))


(defun json->string (json-object)
  ;; json-object is an object like the type returned by string->json
  (java:jcall (java:jmethod "com.google.gson.Gson" "toJson" "java.lang.Object")
              (java:jnew "com.google.gson.Gson") json-object))
