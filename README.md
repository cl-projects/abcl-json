# abcl-json
### _alejandrozf_

A simple JSON library for ABCL using Gson library

You can use the methods from the class "com.google.gson.JsonObject" to manipulate the Json objects.

Attaching a simple session

```
CL-USER> (ql:quickload :abcl-json)
To load "abcl-json":
  Load 1 ASDF system:
    abcl-json
; Loading "abcl-json"
[package abcl-json]
(:ABCL-JSON)
CL-USER> (abcl-json::string->json "{\"a\": 1, \"b\": 2, \"c\": 3}")
#<com.google.gson.JsonObject {"a":1,"b":2,"c":3} {D8E32C06}>
CL-USER> (abcl-json:json->string *)
"{\"a\":1,\"b\":2,\"c\":3}"
CL-USER>
```

## License

MIT
