;;;; package.lisp

(defpackage #:abcl-json
  (:use #:cl)
  (:export #:string->json #:json->string))
