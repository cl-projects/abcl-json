;;;; abcl-json.asd

(asdf:defsystem #:abcl-json
  :description "A simple JSON library for ABCL"
  :author "alejandrozf"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :components ((:mvn "com.google.code.gson/gson/2.10.1")
               (:file "package")
               (:file "abcl-json")))
